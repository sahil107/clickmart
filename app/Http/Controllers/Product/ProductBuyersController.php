<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\ApiController;

use App\Models\Product;


class ProductBuyersController extends ApiController
{
    public function index(Product $product)
    {
        $buyers = $product->transactions()
            ->with('buyer')
            ->get()
            ->pluck('buyer')
            ->unique()
            ->values();
        return $this->showAll($buyers);
    }
}
