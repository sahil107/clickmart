<?php

namespace App\Http\Controllers;


use App\Traits\ApiResponser as TraitsApiResponser;


class ApiController extends Controller
{
    use TraitsApiResponser;
}
