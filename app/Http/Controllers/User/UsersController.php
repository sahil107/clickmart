<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\ApiController;

use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends ApiController
{

    public function index()
    {
        $users = User::all();
        return $this->showAll($users);
    }

    public function store(Request $request)
    {
        $rules = [
            'name'=> 'required',
            'password'=>'required|min:8|max:255|confirmed',
            'email'=>'required|email|unique:users',
        ];
        $this->validate($request, $rules);

        $data = $request->all();
        $data['password']= bcrypt($request->password);
        $data['verified']= User::UNVERIFIED_USER;
        $data['verification_token'] = User::generateVerificationToken();
        $data['admin'] = User::REGULAR_USER;

        $user = User::create($data);
        return response()->json(['data'=>$user], 201);
    }

    public function show(User $user)
    {
        return $this->showOne($user);
    }

    public function update(Request $request, User $user)
    {
        $rules = [
            'email'=>'email|unique:users,email,'.$user->id,
            'password'=>'min:8|max:255|confirmed',
            'admin'=>'in:'.User::REGULAR_USER.','.User::ADMIN_USER,
        ];
        $this->validate($request, $rules);
        if($request->has('name')){
            $user->name = $request->name;
        }
        if($request->has('email')){
            $user->email = $request->email;
            $user->verified = User::UNVERIFIED_USER;
            $user->verification_token = User::generateVerificationToken();
        }
        if($request->has('password')){
            $user->password = bcrypt($request->password);

        }
        if($request->has('admin')){
            if(!$user->isVerified()){
                return response()->json(['error'=>'Only verified users can modify the admin field'], 409);
            }

            $user->admin = $request->admin;
        }
        if(!$user->isDirty()){
            return response()->json(['error'=>'You need to specify a different values!'], 422);
        }

        $user->save();
        return response()->json(['data'=>$user], 200);
    }

    public function destroy(User $user)
    {
        $user->delete();

        return $this->showOne(new User(), 204);  //204 status code means 'No Content'
        //while deleting a resource following this practice to pass empty object and showing status 204 should be considered a good practice
    }

    public function verify(string $token)
    {
        $user = User::where('verification_token', $token)->firstOrFail();
        $user->verified = User::VERIFIED_USER ;
        $user->verification_token = null;
        $user->save();
        return $this->showMessage("The account has been verified");
    }
}
