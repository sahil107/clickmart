<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\ApiController;

use App\Models\Category;


class CategoryTransactionsController extends ApiController
{
    public function index(Category $category)
    {
        $transactions = $category->products()
                                ->whereHas('transactions')
                                ->with('transactions')
                                ->get()
                                ->pluck('transactions')
                                ->flatten();
        return $this->showAll($transactions);
    }
}
