<?php

namespace App\Http\Controllers\Category;

use App\Http\Controllers\ApiController;
use App\Models\Category;


class CategoryProductController extends ApiController
{
    public function index(Category $category)
    {
        $products = $category->products()->get();
        return $this->showAll($products);
    }
}
