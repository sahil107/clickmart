<?php

namespace App\Http\Controllers\Buyer;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Buyer;
use Illuminate\Http\Request;

class BuyerSellerController extends ApiController
{
    public function index(Buyer $buyer)
    {
        $sellers = $buyer->transactions()
        ->with('product.seller')
        ->get()
        ->pluck('product.seller')
        ->unique();  //this will prevent printing the seller two times if a buyer has bought its 2 products!

        return $this->showAll($sellers);
    }
}
