<?php
namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Collection;
trait ApiResponser
{
    private function successResponse($data, $code)
    {
        return response()->json($data, $code);
    }

    public function errorResponse($message, $code){
        return response()->json(['error'=>$message, 'code'=> $code], $code);
    }

    public function showAll(Collection $collection, int $code = 200)
    {
        if($collection->isEmpty()){
            return $this->successResponse(['data'=>$collection], $code);
        }
        $transformer = $collection->first()->transformer;
        $data = $this->transformData($collection, $transformer);
        return $this->successResponse($data, $code);
    }

    public function showOne(Model $model, int $code = 200)
    {
        if(empty($model->toArray())){
            return $this->successResponse(['data'=>$model], $code);
        }
        $transformer = $model->transformer;
        $data = $this->transformData($model, $transformer);
        return $this->successResponse($data, $code);
    }

    public function showMessage($message, $code=200)
    {
        return response()->json(['data'=>$message], $code);
    }

    private function transformData($data, $transformer) :array
    {
        $transformedCollection = fractal($data, new $transformer);
        return $transformedCollection->toArray();
    }
}


