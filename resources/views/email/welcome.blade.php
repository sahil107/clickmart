<x-mail::message>
# Hello {{ $user->name }}

Thank you for creating account on our platform! Please verfify your account using the following link!

<x-mail::button :url="$verificationURL">
Verify Account
</x-mail::button>

Thanks,<br>
{{ config('app.name') }}
</x-mail::message>
