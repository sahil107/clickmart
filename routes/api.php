<?php

use App\Http\Controllers\Buyer\BuyerCategoriesController;
use App\Http\Controllers\Buyer\BuyerProductController;

use App\Http\Controllers\Buyer\BuyerSellerController;
use App\Http\Controllers\Buyer\BuyerTransactionController;
use App\Http\Controllers\Category\CategoryBuyersController;
use App\Http\Controllers\Category\CategoryProductController;
use App\Http\Controllers\Category\CategorySellerController;
use App\Http\Controllers\Category\CategoryTransactionsController;
use App\Http\Controllers\Product\ProductBuyersController;
use App\Http\Controllers\Product\ProductBuyerTransactionsController;
use App\Http\Controllers\Product\ProductCategoriesController;
use App\Http\Controllers\Product\ProductTransactionsController;
use App\Http\Controllers\Seller\SellerBuyersController;
use App\Http\Controllers\Seller\SellerCategoriesController;
use App\Http\Controllers\Seller\SellerProductsController;
use App\Http\Controllers\Seller\SellerTransactionsController;
use App\Http\Controllers\Transaction\TransactionCategoryController;
use App\Http\Controllers\Transaction\TransactionSellerController;
use App\Http\Controllers\User\UsersController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/




Route::resource('users', \App\Http\Controllers\User\UsersController::class, ['except'=>['create', 'edit']]);
Route::resource('buyers', \App\Http\Controllers\Buyer\BuyersController::class, ['only'=>['index', 'show']]);
Route::resource('sellers', \App\Http\Controllers\Seller\SellersController::class, ['only'=>['index', 'show']]);
Route::resource('categories', \App\Http\Controllers\Category\CategoriesController::class, ['except'=>['create', 'edit']]);
Route::resource('products', \App\Http\Controllers\Product\ProductsController::class, ['only'=>['index', 'show']]);
Route::resource('transactions', \App\Http\Controllers\Transaction\TransactionsController::class, ['only'=>['index', 'show']]);

Route::resource('transactions.categories', TransactionCategoryController::class)->only(['index']);
Route::resource('transactions.seller', TransactionSellerController::class)->only(['index']);

Route::resource('buyers.transactions', BuyerTransactionController::class)->only(['index']);
Route::resource('buyers.products', BuyerProductController::class)->only(['index']);
Route::resource('buyers.sellers', BuyerSellerController::class)->only(['index']);
Route::resource('buyers.categories', BuyerCategoriesController::class)->only(['index']);

Route::resource('categories.products', CategoryProductController::class)->only(['index']);
Route::resource('categories.sellers', CategorySellerController::class)->only(['index']);
Route::resource('categories.transactions', CategoryTransactionsController::class)->only(['index']);
Route::resource('categories.buyers', CategoryBuyersController::class)->only(['index']);

Route::resource('sellers.transactions', SellerTransactionsController::class)->only(['index']);
Route::resource('sellers.categories', SellerCategoriesController::class)->only(['index']);
Route::resource('sellers.buyers', SellerBuyersController::class)->only(['index']);
Route::resource('sellers.products', SellerProductsController::class)->except(['create', 'edit']);

Route::resource('products.transactions', ProductTransactionsController::class)->only(['index']);
Route::resource('products.buyers', ProductBuyersController::class)->only(['index']);
Route::resource('products.categories', ProductCategoriesController::class)->only(['index', 'update', 'destroy']);

Route::resource('products.buyers.transactions', ProductBuyerTransactionsController::class)->only(['store']);

Route::get('users/verify/{token}', [UsersController::class, 'verify'])->name('verify');

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
